# Création du VPC
resource "aws_vpc" "VPC" {
  cidr_block = "10.0.2.0/24"
  tags = {
    Name = "${var.environment}-app-vpc"
  }
}

# Création du sous-réseau (de 10.2.0.0 à 10.2.0.62)
resource "aws_subnet" "my_subnet" {
  vpc_id             = aws_vpc.VPC.id
  cidr_block         = "10.0.2.0/26"
  availability_zone  = "eu-west-1a"
  tags = {
    Name = "${var.environment}-app-subnet1"
  }
}

# Création du second sous-réseau (de 10.2.0.128 à 10.2.0.190) (minimum 2 sous-réseaux pour un load balancer)
resource "aws_subnet" "my_subnet2" {
  vpc_id             = aws_vpc.VPC.id
  cidr_block         = "10.0.2.128/26"
  availability_zone  = "eu-west-1b"
  tags = {
    Name = "${var.environment}-app-subnet2"
  }
}


# Création de la passerelle internet
resource "aws_internet_gateway" "gateway" {
  vpc_id = aws_vpc.VPC.id

  tags = {
    Name = "${var.environment}-app-igw"
  }
}

# Configuration de la table de routage du VPC avec la passerelle Internet
resource "aws_route" "route_igw" {
	route_table_id = aws_vpc.VPC.default_route_table_id

	destination_cidr_block = "0.0.0.0/0"
	gateway_id = aws_internet_gateway.gateway.id

}

# Création du load Balancer (équilibreur de charge)
resource "aws_lb" "load_balancer" {
  name               = "${var.environment}-app-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.all_traffic.id]
  subnets            = [aws_subnet.my_subnet.id, aws_subnet.my_subnet2.id]
}

# Création d'une ressource Target Group (Cible du Load Balancer)
resource "aws_lb_target_group" "app_target_group" {
  name        = "${var.environment}-app-lb-tg"
  port        = 80
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = aws_vpc.VPC.id
}

# Fournit une ressource Listener dans le LoadBalancer (écoute sur un port et redirige vers un Target Group)
resource "aws_lb_listener" "front_end" {
  load_balancer_arn = aws_lb.load_balancer.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.app_target_group.arn
  }
}


